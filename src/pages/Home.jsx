import { useState } from 'react';
import { Buscador } from '../components/buscador/Buscador';
import { Banner } from '../components/navbar/Banner';
import { ListadoPersonajes } from '../components/personajes/ListadoPersonajes';

export function Home() {
  let [buscador, setBuscador] = useState('');
  return (
    <div>
      <Banner />
      <div className='px-5'>
        <h1 className='py-4'> Personajes destacados </h1>

        <Buscador valor={buscador} onBuscar={setBuscador} />

        <ListadoPersonajes buscar={buscador} />
      </div>
    </div>
  );
}

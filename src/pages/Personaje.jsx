import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { ListaEpisodio } from '../components/episodio/ListaEpisodio';
import { BannerDetallePersonaje } from '../components/personajes/BannerDetallePersonaje';

export function Personaje() {
  let { personajeId } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
      });
  }, []);

  useEffect(() => {
    if (personaje) {
      let peticionesEpisodio = personaje.episode.map((episodio) => {
        return axios.get(episodio);
      });

      Promise.all(peticionesEpisodio).then((respuestas) => {
        setEpisodios(respuestas);
      });
    }
  }, [personaje]);

  return (
    <div className='p-5'>
      {personaje ? (
        <div>
          <BannerDetallePersonaje {...personaje} />

          <h2 className='py-4'>Episodios</h2>

          {episodios ? (
            <ListaEpisodio episodios={episodios} />
          ) : (
            <div>Cargando episodios...</div>
          )}
        </div>
      ) : (
        <div>Cargando...</div>
      )}
    </div>
  );
}

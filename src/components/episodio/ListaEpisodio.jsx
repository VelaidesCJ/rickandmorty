import { Episodio } from './Episodio';

export function ListaEpisodio({ episodios }) {
  return (
    <div className='row bg-dark p-3'>
      {episodios.map((episodio) => (
        <Episodio key={episodio.data.id} {...episodio.data} />
      ))}
    </div>
  );
}

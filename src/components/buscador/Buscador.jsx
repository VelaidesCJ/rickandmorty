export function Buscador({ valor, onBuscar }) {
  return (
    <div className='d-flex justify-content-end mb-5'>
      <div className='mb-3 col-5 shadow-lg '>
        <input
          type='email'
          className='form-control'
          id='exampleFormControlInput1'
          placeholder='Buscar personaje...'
          value={valor}
          onChange={(evento) => onBuscar(evento.target.value)}
        />
      </div>
    </div>
  );
}

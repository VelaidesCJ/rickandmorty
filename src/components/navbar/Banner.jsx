import bannerImg from '../../assets/img/RaM.jpg';

export function Banner() {
  return (
    <div>
      <img
        style={{ height: '700px', objectFit: 'fill' }}
        src={bannerImg}
        className='card-img'
        alt='banner'
      />
    </div>
  );
}

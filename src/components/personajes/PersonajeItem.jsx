import { Link } from 'react-router-dom';

function getEstilosStatus(color = 'green') {
  const estiloCirculo = {
    width: '10px',
    height: '10px',
    display: 'inline-block',
    backgroundColor: color,
    borderRadius: '50%',
    marginRight: '10px',
  };

  return estiloCirculo;
}

export function PersonajeItem({
  id,
  name,
  status,
  species,
  location,
  image,
  origin,
}) {
  let color = 'green';

  if (status === 'unknown') {
    color = 'gray';
  }

  if (status === 'Dead') {
    color = 'red';
  }

  return (
    <div className='col-3'>
      <Link
        to={`/personaje/${id}`}
        style={{ textDecoration: 'none', color: 'initial' }}
      >
        <div className='card mb-3' style={{ maxWidth: '540px' }}>
          <div className='row g-0'>
            <div className='col-md-4'>
              <img
                style={{ height: '100%', objectFit: 'cover' }}
                src={image}
                className='img-fluid rounded-start'
                alt={name}
              />
            </div>
            <div className='col-md-8'>
              <div className='card-body'>
                <h5 className='card-title mb-0'> {name} </h5>
                <p>
                  <span style={getEstilosStatus(color)}></span>
                  {status} - {species}
                </p>
                <p className='mb-0 text-muted'>Last know location:</p>
                <p>{location?.name}</p>

                <p className='mb-0 text-muted'>Origin:</p>
                <p>{origin?.name}</p>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}

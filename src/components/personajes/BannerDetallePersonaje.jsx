function getEstilosStatus(status) {
  let color = 'green';

  if (status === 'unknown') {
    color = 'gray';
  }

  if (status === 'Dead') {
    color = 'red';
  }

  const estiloCirculo = {
    width: '10px',
    height: '10px',
    display: 'inline-block',
    backgroundColor: color,
    borderRadius: '50%',
    marginRight: '10px',
  };

  return estiloCirculo;
}

export function BannerDetallePersonaje({
  name,
  status,
  species,
  location,
  image,
  origin,
}) {
  return (
    <div className='bg-dark p-3'>
      <div className='card  '>
        <div className='row g-0'>
          <div className='col-md-8' style={{ height: '450px' }}>
            <img
              style={{ height: '100%', objectFit: 'fill', width: '100%' }}
              src={image}
              className='img-fluid rounded-start'
              alt={name}
            />
          </div>
          <div className='col-md-4'>
            <div className='card-body'>
              <h1 className='card-title mb-0 mt-5'> {name} </h1>
              <p>
                <span style={getEstilosStatus(status)}></span>
                {status} - {species}
              </p>
              <p className='mb-0 text-muted h3 mt-5'>Last know location:</p>
              <p className='h1 '>{location?.name}</p>

              <p className='mb-0 text-muted h3 mt-5'>Origin:</p>
              <p className='h1'>{origin?.name}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

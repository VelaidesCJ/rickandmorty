import axios from 'axios';
import { useEffect, useState } from 'react';
import { PersonajeItem } from './PersonajeItem';

export function ListadoPersonajes({ buscar }) {
  const [personajes, setPersonajes] = useState(null);
  let personajesFiltrados = personajes?.results;

  if (buscar && personajes) {
    personajesFiltrados = personajes.results.filter((personaje) => {
      let nombrePersonajeMinuscula = personaje.name.toLowerCase();
      let buscadorMinuscula = buscar.toLowerCase();
      return nombrePersonajeMinuscula.includes(buscadorMinuscula);
    });
  }

  useEffect(() => {
    axios.get('https://rickandmortyapi.com/api/character').then((respuesta) => {
      setPersonajes(respuesta.data);
    });
  }, []);

  return (
    <div className='row py-5 bg-dark'>
      {personajes
        ? personajesFiltrados.map((personaje) => {
            return <PersonajeItem {...personaje} key={personaje.id} />;
          })
        : 'Cargando...'}
    </div>
  );
}

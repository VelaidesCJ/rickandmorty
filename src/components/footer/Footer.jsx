export function Footer() {
  return (
    <div className='navbar-light bg-dark mt-3'>
      <p className='text-center text-white py-3 mb-0'>
        {' '}
        Project created by Jair Calderón Velaides CARD IV - 2022{' '}
      </p>
    </div>
  );
}
